import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    modal: null,
    item: {},
    cartItems: [],
    counter: 0,
    spinner: false,
    spinnerMessage: null
}

const addtoCounter = (state) => {
    return state.counter = state.cartItems.reduce((prev, cur) => prev + cur.quantity, 0)  
};

const cartSlice = createSlice({
    name: 'cart',
    initialState,
    reducers: {
        openModal(state, action){
            state.item = action.payload;
            state.modal = 'detail';            
        },
        closeModal(state){
            state.modal = null;
        },
        addToCart(state, action){
            if(state.cartItems.filter(item => item.item.code === action.payload.item.code).length > 0){
                state.cartItems.find(item => item.item.code === action.payload.item.code).quantity += action.payload.quantity;
            } else {
                state.cartItems.push({
                    item: action.payload.item,
                    quantity: action.payload.quantity
                }) 
            } 
            addtoCounter(state);         
        },
        setCart(state, action){
            state.cartItems = action.payload;
            addtoCounter(state);
        },
        addToItem(state, action){
            state.cartItems.find(item => item.item.code === action.payload.item.code).quantity++;
            addtoCounter(state);  
        },
        minusToItem(state,action){
            if(action.payload.quantity === 1){
                state.cartItems = state.cartItems.filter(item => item.item.code !== action.payload.item.code);
            } else{
                state.cartItems.find(item => item.item.code === action.payload.item.code).quantity--;
            }
            addtoCounter(state);  
        },
        openCartModal(state){
            state.modal = 'cart';
        },   
        openSpinner(state, action){
            state.spinner = true;
            state.spinnerMessage = action.payload;
        },
        closeSpinner(state){
            state.spinner = false;
            state.spinnerMessage = null;
        }    
    }
});


export const cartActions = cartSlice.actions;

export default cartSlice.reducer;