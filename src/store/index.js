import { configureStore } from '@reduxjs/toolkit';

import authReducer from './authStore/index';
import itemReducer from './itemsStore/index';
import cartReducer from './cartStore/index';

const store = configureStore({
    reducer: {
        auth: authReducer,
        items: itemReducer,
        cart: cartReducer
    }
});

export default store;


