import { createSlice } from "@reduxjs/toolkit";


const filterByType = (state, action) => {
  return state.items.filter(
    item => item.type === action.payload);
};

const initialState = {
  type: 'Todos',
  items: [],
  filteredItemsByType: [],
  filteredItemsByName: []
};

const itemsSlice = createSlice({
  name: "items",
  initialState,
  reducers: {
    addItems(state, action) {
      state.items = [...state.items, action.payload];
    },
    filterItemsByType(state, action) {
      if (!action.payload) {
        state.filteredItemsByName = state.items;
        state.filteredItemsByType = state.items;
        state.type = 'Todos';
      } else {
        state.filteredItemsByType = filterByType(state, action);
        state.filteredItemsByName = filterByType(state, action);
        state.type = action.payload;
      }
    },
    filterItemsByName(state, action) {
      if (action.payload) {
        state.filteredItemsByName = state.filteredItemsByType.filter((item) =>
          item.description.toLowerCase().includes(action.payload.toLowerCase())
        );
      } else {
        state.filteredItemsByName = state.filteredItemsByType;
      }
    },
    setItems(state, action) {
      state.items = action.payload;
      state.filteredItems = action.payload;
      state.filteredItemsByName = action.payload;
    },
  },
});

export const itemsActions = itemsSlice.actions;

export default itemsSlice.reducer;
