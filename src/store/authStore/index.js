
import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isAuthenticated: false,
    token : null,
    imgUrl: null,
    userName: null,
    isError: false,
    error: null,
};

const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        authenticate(state, action){
            state.isAuthenticated = true;
            state.token = action.payload.token;
            state.imgUrl = action.payload.imgUrl;
            state.userName = action.payload.userName;
            state.error = null;
            state.isError = false;
        },
        errorAdquired(state, action){
            state.isAuthenticated = false;
            state.token = null;
            state.imgUrl = null
            state.userName = null;
            state.error = action.payload;
            state.isError = true;
        },
        logout(state){
            state.isAuthenticated = false;
            state.token = null;
            state.imgUrl = null;
            state.userName = null;
            state.error = null;
            state.isError = false;
        }
    }
});


export const authActions = authSlice.actions;

export default authSlice.reducer;