import { useDispatch } from "react-redux";
import { cartActions } from "../store/cartStore";
import { useHistory } from "react-router";

const useRedirect = () => {

  const dispatch = useDispatch();  

  const history = useHistory();

  const redirectTo = (destiny, message) => {
    dispatch(cartActions.openSpinner(message));
    setTimeout(() => {
      dispatch(cartActions.closeSpinner());
      if(destiny) history.push(destiny);
    }, 1000);
  };

  return redirectTo;
};

export default useRedirect;
