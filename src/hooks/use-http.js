import { useState, useCallback } from "react";
import { useDispatch } from "react-redux";
import { cartActions } from "../store/cartStore";

const useHttp = (callBackFunc) => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const dispatch = useDispatch();


  const sendRequest = useCallback(
    async (requestConfig, message) => {
      dispatch(cartActions.openSpinner(message));
      setIsLoading(true);
      setError(null);
      try {
        const response = await fetch(requestConfig.url, {
          method: requestConfig.method ? requestConfig.method : "GET",
          headers: requestConfig.headers ? requestConfig.headers : {},
          body: requestConfig.body ? JSON.stringify(requestConfig.body) : null,
        });

        if (!response.ok) {
          throw new Error("Request failed");
        }
        const data = await response.json();

        if(callBackFunc) {
          callBackFunc(data);
        } 
     
      } catch (err) {
        setError(err.message || "Something went wrong");
      }
      setIsLoading(false);
      dispatch(cartActions.closeSpinner());
    },
    [callBackFunc, dispatch]
  );
  return {
    isLoading,
    error,
    sendRequest,
  };
};

export default useHttp;
