import "./App.css";
import { Route, Redirect } from "react-router-dom";
import Main from "./components/Main/main";
import Cart from "./components/cart";
import Header from "./components/layout/header";
import Sidebar from "./components/layout/sidebar";
import { Fragment } from "react";
import Callback from "./components/auth/callback";
import { useEffect, useCallback } from "react";
import { useDispatch } from "react-redux";
import { authActions } from "./store/authStore/index";
import { cartActions } from "./store/cartStore";
import useHttp from "./hooks/use-http";

const url =
  "http://localhost:8081/oauth2/authorize/google?redirect_uri=http://localhost:3000/callback";

function App() {
  const dispatch = useDispatch();
  const cartSetter = useCallback(
    (data) => {
      const transformedData = data.products.map((dataItem) => {
        const { productItem: item, quantity } = dataItem;
        return { item, quantity };
      });
      dispatch(cartActions.setCart(transformedData));
    },
    [dispatch]
  );

  const { sendRequest } = useHttp(cartSetter);

  useEffect(() => {
    const token = localStorage.getItem("token");
    let timer;
    if (token) {
      const payload = JSON.parse(
        decodeURIComponent(escape(window.atob(token.split(".")[1])))
      );
      dispatch(
        authActions.authenticate({
          token: token,
          imgUrl: payload.imgUrl,
          userName: payload.username,
        })
      );
      const remainingTime = payload.exp * 1000 - new Date().getTime();
      timer = setTimeout(() => {
        localStorage.removeItem("token");
        dispatch(authActions.logout());
      }, remainingTime);

      sendRequest(
        {
          url: "http://localhost:8081/cart/cart",
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
        "Fetching User Cart"
      );
    }

    return () => {
      clearTimeout(timer);
    };
  }, [dispatch, sendRequest]);

  return (
    <Fragment>
      <Sidebar />
      <Header />
      <Route path="/" exact>
        <Redirect to="/welcome" />
      </Route>
      <Route
        path="/googleauth"
        component={() => {
          window.location.href = url;
          return null;
        }}
      />
      <Route path="/callback" component={Callback} />

      <Route path="/welcome">
        <Main />
      </Route>
      <Route path="/cart">
        <Cart />
      </Route>
    </Fragment>
  );
}

export default App;
