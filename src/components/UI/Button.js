import classes from './Button.module.css';


const Button = (props) => {

    const content = props.children ? props.children : props.text;
    const width = props.width ? props.width : '2.5rem';
    const height = props.height ? props.height : '2.5rem';
    
    const styles = {
        height: height,
        width: width,
    }

    return <button onClick={props.onClickHandler} className={classes.button} style={styles}>
        {content}
    </button>;
};

export default Button;