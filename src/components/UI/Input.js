import classes from "./Input.module.css";
import { useRef } from "react";

const Input = (props) => {
  const inputRef = useRef();

  return (
    <input
      ref={inputRef}
      onChange={props.onChangeHandler.bind(null, inputRef)}
      className={classes.input}
      placeholder={props.placeholder}
    />
  );
};

export default Input;
