import Button from "./Button";
import classes from "./Finder.module.css";
import Input from "./Input";
import { FaSearchengin } from "react-icons/fa";
import { useDispatch } from "react-redux";
import { itemsActions } from "../../store/itemsStore";




const Finder = (props) => {

  const dispatch = useDispatch();

  const onLookFor = (inputRef) =>{
      dispatch(itemsActions.filterItemsByName(inputRef.current.value))  
  };

  return <div className={classes.finder}>
    <Input onChangeHandler={onLookFor} placeholder={props.placeholder}/>
    <Button><FaSearchengin size={18}/></Button>    
  </div>;
};

export default Finder;
