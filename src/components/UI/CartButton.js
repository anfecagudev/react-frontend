import classes from "./CartButton.module.css";
import cartlogo from "../../assets/shopping-cart-icon.png";
import Badge from "./Badge";
import { useSelector, useDispatch } from "react-redux";
import { cartActions } from "../../store/cartStore";

const CartButton = (props) => {

  const cartQuantity = useSelector(state => state.cart.counter);

  const dispatch = useDispatch();

  const openCartModal = () => {
    dispatch(cartActions.openCartModal())
  }

  return (
    <div onClick={openCartModal} className={classes.cartlogo}>
      <Badge>{cartQuantity}</Badge>  
      <img className={classes.cartlogoimage} src={cartlogo} alt="CartLogo" />
    </div>
  );
};

export default CartButton;
