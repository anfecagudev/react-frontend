import classes from './Badge.module.css';
import { useEffect, useState } from 'react';

const Badge = (props) => {
    
    
    const [css, setCss] = useState([
        classes.badge
    ])  

    useEffect(() => {
        setCss(state => [...state, classes.bump]);
        const timer = setTimeout( () => {
            setCss([classes.badge])
        }, 100)
        return () => {
            clearTimeout(timer);
        }
    },[props]);
    
    return <div className={css.join(' ')}>
        {props.children}
    </div>
}; 

export default Badge;