import classes from "./itemModal.module.css";
import { useSelector, useDispatch } from "react-redux";
import { cartActions } from "../../store/cartStore";
import useHttp from "../../hooks/use-http";


const ItemModal = (props) => {
  const dispatch = useDispatch();

  const url = "http://localhost:8081/cart/savecart";

  const token = useSelector((state) => state.auth.token);

  const counter = useSelector(state => state.cart.counter);

  const { sendRequest } = useHttp();

  const addHandler = () => {
    dispatch(cartActions.addToItem(props.item));
  };

  const removeHandler = () => {
    dispatch(cartActions.minusToItem(props.item));
    if(counter === 1) {
      sendRequest(
        {
          url: url,
          method: "POST",
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
          body: [],
        },
        "Emtpying User Cart..."
      );
        dispatch(cartActions.closeModal());
    }
  }; 

  return (
    <div className={classes.container}>
      <div className={classes.imgContainer}>
        <img src={props.item.item.imageUrl} alt="imagen descriptiva" />
      </div>
      <div className={classes.dataholder}>
        <p>{props.item.item.description}</p>
        <p className={classes.unidad}>Unidad:</p>
        <p>$ {props.item.item.price}</p>
      </div>
      <div className={classes.quantityContainer}>
        <button onClick={removeHandler} className={classes.white_button}>
          -
        </button>
        <p>{props.item.quantity}</p>
        <button onClick={addHandler} className={classes.green_button}>
          +
        </button>
      </div>
      <div className={classes.subtotal}>
        $ {props.item.quantity * props.item.item.price}
      </div>
    </div>
  );
};

export default ItemModal;
