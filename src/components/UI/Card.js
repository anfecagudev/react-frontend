import Dialog from "../layout/dialog";
import classes from "./Card.module.css";
import Button from "./Button";
import React from 'react';

const Card = (props) => {

  return (
    <div className={classes.card}>
      <div className={classes.imgcontainer}>
        <Dialog>
          <Button onClickHandler={props.onClickHandler} width="4rem">Ver</Button>
          <Button onClickHandler={props.onAddItemHandler} width="5rem">Agregar</Button>
        </Dialog>
        <img src={props.imgsrc} alt="product" />
      </div>

      <h3>{props.description}</h3>
      <h5>SKU: {props.code}</h5>
      <h1>$ {props.price}</h1>
      <h4>gramo a $ {props.price / 1000}</h4>
    </div>
  );
};

export default React.memo(Card);
