import classes from "./main.module.css";
import { useSelector } from "react-redux";
import Card from "../UI/Card";
import FullScreenDialog from "../layout/fullscreendialog";
import ViewModal from "../layout/viewmodal";
import { useDispatch } from "react-redux";
import { cartActions } from "../../store/cartStore/index";
import { Transition } from "react-transition-group";
import CartModal from "../layout/cartModal";
import { useEffect, useCallback } from "react";
import useHttp from "../../hooks/use-http";
import { itemsActions } from "../../store/itemsStore";
import Spinner from "../layout/spinner";
import useRedirect from "../../hooks/use-redirect";

const Main = () => {
  const isAuth = useSelector((state) => state.auth.isAuthenticated);

  const dispatch = useDispatch();
  const redirect = useRedirect();
  const isSpinner = useSelector(state => state.cart.spinner);

  const transformData = useCallback(
    (data) => {
      dispatch(itemsActions.setItems(data));
    },
    [dispatch]
  );

  const { sendRequest } = useHttp(transformData);

  useEffect(() => {
    sendRequest(
      {
        url: "http://localhost:8081/users/datalist",
      },
      "Fetching items from database"
    );
  }, [sendRequest]);

  const products = useSelector((state) => state.items.filteredItemsByName);
  const modal = useSelector((state) => state.cart.modal);
  const spinner = useSelector((state) => state.cart.spinner);

  const onViewItem = useCallback(
    (product) => {
      dispatch(cartActions.openModal(product));
    },
    [dispatch]
  );

  const onAddItem = useCallback(
    (product) => {
      if (isAuth) {
        dispatch(
          cartActions.addToCart({
            item: product,
            quantity: 1,
          })
        );
      } else {
        redirect("/googleauth");
      }
    },
    [isAuth, dispatch, redirect]
  );

  const title = useSelector(state => state.items.type);

  return (
    <div className={classes.main}>
      <Transition in={!!modal} timeout={300} unmountOnExit mountOnEnter>
        {(state) => (
          <FullScreenDialog show={state}>
            <Transition in={modal === "detail"} timeout={300} unmountOnExit>
              {(state) => <ViewModal show={state} />}
            </Transition>
            <Transition in={modal === "cart"} timeout={300} unmountOnExit>
              {(state) => <CartModal show={state} />}
            </Transition>
          </FullScreenDialog>
        )}
      </Transition>
      <Transition in={spinner} timeout={300} unmountOnExit>
        {(state) => <Spinner destiny="Login" />}
      </Transition>
      <h1 className={classes.title}>{title}</h1>
      {!isSpinner && (
        <div className={classes.card_container}>
          {products.map((product) => {
            return (
              <Card
                onClickHandler={() => {
                  onViewItem(product);
                }}
                onAddItemHandler={() => onAddItem(product)}
                key={product.code}
                imgsrc={product.imageUrl}
                description={product.description}
                code={product.code}
                price={product.price}
              />
            );
          })}
        </div>
      )}
    </div>
  );
};

export default Main;
