import Button from "../UI/Button";
import CartButton from "../UI/CartButton";
import Finder from "../UI/Finder";
import styles from "./header.module.css";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import AuthUser from "./authuser";
import { useState } from "react";
import { Transition } from "react-transition-group";
import Accordion from "./accordion";
import { authActions } from "../../store/authStore";
import { cartActions } from "../../store/cartStore";

const Header = (props) => {
  const history = useHistory();
  const isAuth = useSelector((state) => state.auth.isAuthenticated);
  const username = useSelector((state) => state.auth.userName);
  const imgUrl = useSelector((state) => state.auth.imgUrl);
  const [isButtonVisible, setIsButtonVisible] = useState(false);
  const dispatch = useDispatch();

  let timer;

  const fatherHoverHandler = () => {
    clearTimeout(timer);
    setIsButtonVisible(true);
  };

  const logoutHandler = () => {
    localStorage.removeItem('token');
    dispatch(authActions.logout());
    dispatch(cartActions.setCart([]))
  };

  const fatherUnhoverHandler = () => {
    timer = setTimeout(() => {
      setIsButtonVisible(false);
    }, 200);
  };

  const onSignInHandler = () => {
    history.push("/googleauth");
  };

  return (
    <nav className={styles.nav}>
      <Finder placeholder="Buscar Productos" />
      <div className={styles.rightpos}>
        <CartButton />
        {!isAuth && (
          <Button
            onClickHandler={onSignInHandler}
            width="7rem"
            text="Iniciar Sesion"
          />
        )}
        {isAuth && (
          <div
            onMouseEnter={fatherHoverHandler}
            onMouseLeave={fatherUnhoverHandler}
            className={styles.authContainer}
          >
            <AuthUser username={username} imgUrl={imgUrl} />
            <Transition
              in={isButtonVisible}
              timeout={300}
              unmountOnExit
              mountOnEnter
            >
              {(state) => <Accordion onClickHandler={logoutHandler} show={state} />}
            </Transition>
          </div>
        )}
      </div>
    </nav>
  );
};

export default Header;
