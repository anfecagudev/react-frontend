import classes from "./cartModal.module.css";
import { useSelector } from "react-redux";
import ItemModal from "../UI/itemModal";
import Totalizer from "./totalizer";
import { TransitionGroup, CSSTransition } from "react-transition-group";

const CartModal = (props) => {
  const cartItems = useSelector((state) => state.cart.cartItems);

  const cssClasses = [
    classes.modal,
    props.show === "entered" ? classes.modal_open : "",
    props.show === "exiting" ? classes.modal_closed : "",
  ];

  return (
    <div className={cssClasses.join(" ")}>
      <TransitionGroup className={classes.testing}>
        {cartItems.map((item) => {
          return (
            <CSSTransition
              key={item.item.code}
              classNames={{
                enterActive: classes.enterActive,
                enterDone: classes.enter,
                exitActive: classes.exitActive,
                exitDone: classes.exit,
              }}
              timeout={300}
            >
              <ItemModal item={item} />
            </CSSTransition>
          );
        })}
      </TransitionGroup>
      <hr className={classes.hr} />
      <Totalizer show={props.show}></Totalizer>
    </div>
  );
};

export default CartModal;
