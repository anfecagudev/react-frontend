import classes from "./totalizer.module.css";
import { useSelector } from "react-redux";
import useHttp from "../../hooks/use-http";
import useRedirect from "../../hooks/use-redirect";
import { useDispatch } from "react-redux";
import { cartActions } from "../../store/cartStore";

const Totalizer = (props) => {
  const url = "http://localhost:8081/cart/savecart";

  const token = useSelector((state) => state.auth.token);

  const items = useSelector((state) => state.cart.cartItems);

  const isAuth = useSelector((state) => state.auth.isAuthenticated);

  const dispatch = useDispatch();

  const redirect = useRedirect();

  const total = items.reduce(
    (prev, cur) => prev + cur.quantity * cur.item.price,
    0
  );

  const { sendRequest } = useHttp();

  const cssClasses = [
    classes.container,
    props.show === "entered" ? classes.container_open : "",
    props.show === "exiting" ? classes.container_closed : "",
  ];

  const shoppingHandler = () => {
    if (isAuth) {
      const products = items.map((item) => {
        return {
          productItem: item.item,
          quantity: item.quantity,
        };
      });
      sendRequest(
        {
          url: url,
          method: "POST",
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
          body: products,
        },
        "Saving User Cart..."
      );
      dispatch(cartActions.closeModal());
    } else {
      redirect(
        "/googleauth",
        "No se encuentra autentificado. Redireccionando ..."
      );
    }
  };

  return (
    <div className={cssClasses.join(" ")}>
      <div className={classes.shoppingBtn}>
        <button onClick={shoppingHandler} className={classes.button}>
          Comprar
        </button>
      </div>
      <div className={classes.rightAligner}>
        <div>Total:</div>
        <div>$ {total}</div>
      </div>
    </div>
  );
};

export default Totalizer;
