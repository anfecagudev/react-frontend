import classes from "./accordion.module.css";

const Accordion = (props) => {
  const cssClasses = [
    classes.logoutButton,
    props.show === "entered" ? classes.logoutButtonOpened: "",
    props.show === "exiting" ? classes.logoutButtonClosed : "",
  ];

  return (

      <div className={cssClasses.join(' ')}>
        <button onClick={props.onClickHandler} className={classes.button}>Logout</button>
      </div>
 
  );
};

export default Accordion;
