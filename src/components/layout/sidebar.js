import classes from "./sidebar.module.css";
import { useDispatch } from "react-redux";
import { itemsActions } from "../../store/itemsStore";

const Sidebar = (props) => {
  const dispatch = useDispatch();

  const filterItems = (filterWord) => {
    dispatch(itemsActions.filterItemsByType(filterWord));
  };

  const sections = [
    { name: "Todos", payload: null },
    { name: "Carnes", payload: "Carnes" },
    { name: "Tecnologia", payload: "Tecnologia" },
  ];

  return (
    <div className={classes.sidebar}>
      <h1>Secciones</h1>
      {sections.map(section => {
        return (
          <button key={section.name} onClick={filterItems.bind(null, section.payload)}>
            {section.name}
          </button>
        );
      })}
    </div>
  );
};

export default Sidebar;
