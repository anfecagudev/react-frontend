import classes from "./dialog.module.css";

const Dialog = (props) => {
    return (
  <div className={classes.dialog}>
    <div className={classes.backdrop}></div>
    <div className={classes.content}>{props.children}</div>
  </div>);
};

export default Dialog;
