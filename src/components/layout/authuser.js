import classes from './authuser.module.css';

const AuthUser = props => {
    return(
    <div className={classes.portrait}>        
        <img src={props.imgUrl} alt="userportrait" />
        <p>{props.username}</p>
    </div>)
};


export default AuthUser;