import Button from "../UI/Button";
import classes from "./viewmodal.module.css";
import { useSelector } from "react-redux";
import { useRef, useState } from "react";
import { IoClose } from "react-icons/io5";
import { useDispatch } from "react-redux";
import { cartActions } from "../../store/cartStore/index";
import useRedirect from "../../hooks/use-redirect";
import React from "react";

const ViewModal = (props) => {
  const isAuth = useSelector((state) => state.auth.isAuthenticated);
  const quantityRef = useRef();
  const [quantity, setQuantity] = useState(1);
  const item = useSelector((state) => state.cart.item);
  const dispatch = useDispatch();
  const redirect = useRedirect();
  const changeHandler = () => {
    setQuantity(quantityRef.current.value);
  };

  const addHandler = () => {
    setQuantity((state) => state + 1);
  };

  const removeHandler = () => {
    setQuantity((state) => state - 1);
  };

  const closeModal = () => {
    dispatch(cartActions.closeModal());
  };

  const cssClasses = [
    classes.modal,
    props.show === "entered" ? classes.modal_open : "",
    props.show === "exiting" ? classes.modal_closed : "",
  ];

  const onAddToCart = () => {
    if (isAuth) {
      dispatch(
        cartActions.addToCart({
          item: item,
          quantity: +quantityRef.current.value,
        })
      );
      dispatch(cartActions.closeModal());
    } else {
      redirect('/googleauth', 'No se encuentra autentificado. Redireccionando ...');
    }
  };

  return (
    <div className={cssClasses.join(" ")}>
      <div className={classes.imgcontainer}>
        <img src={item.imageUrl} alt={props.alttext}></img>
      </div>
      <div className={classes.contentcontainer}>
        <div className={classes.closing_button_container}>
          <button onClick={closeModal} className={classes.closing_button}>
            <IoClose size="2rem" />
          </button>
        </div>
        <h1>{item.description}</h1>
        <p className={classes.h3}>SKU: {item.code}</p>
        <span>
          $ {item.price} /{item.unit}
        </span>
        <hr />
        <div className={classes.quantity_container}>
          <button onClick={removeHandler} className={classes.white_button}>
            -
          </button>
          <input
            min="1"
            type="number"
            ref={quantityRef}
            value={quantity}
            onChange={changeHandler}
          />
          <button onClick={addHandler} className={classes.green_button}>
            +
          </button>
        </div>
        <div className={classes.button_container}>
          <Button onClickHandler={onAddToCart} width="20rem">
            Anadir al Carrito
          </Button>
        </div>
      </div>
    </div>
  );
};

export default React.memo(ViewModal);
