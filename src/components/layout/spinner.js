import classes from "./spinner.module.css";
import { useSelector } from "react-redux";

const Spinner = props => {

  const message = useSelector(state => state.cart.spinnerMessage);

  return (
    <div className={classes.spinnerContainer}>
      <div className={classes.messageContainer}>
          <p>{message}</p>
      </div>
      <div className={classes.loader}></div>
    </div>
  );
};

export default Spinner;
