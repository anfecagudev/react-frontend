import classes from "./fullscreendialog.module.css";
import ReactDom from "react-dom";
import { useDispatch } from "react-redux";
import { cartActions } from "../../store/cartStore/index";
import React from 'react';

const FullScreenDialog = (props) => {
  const dispatch = useDispatch();

  const root = document.getElementById("root");
  const closeModal = () => {
    dispatch(cartActions.closeModal());
  };


  const cssClasses = [
    classes.dialog,
    props.show === "entered" 
    ? classes.dialog_open 
    : '',
    props.show === "exiting"
    ? classes.dialog_closed
    : ''
  ];

  return ReactDom.createPortal(
    <div className={cssClasses.join(' ')}>
      <div onClick={closeModal} className={classes.backdrop}></div>
      {props.children}
    </div>,
    root
  );
};

export default React.memo(FullScreenDialog);
