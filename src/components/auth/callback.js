import { Fragment } from "react";
import { useDispatch } from "react-redux";
import { authActions } from "../../store/authStore/index";
import { useHistory } from "react-router-dom";

const Callback = (props) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const querystring = window.location.search;
  const params = new URLSearchParams(querystring);
  const token = params.get("token");
  if (token) {
    const payload = JSON.parse(
      decodeURIComponent(escape(window.atob(token.split(".")[1])))
    );
    dispatch(
      authActions.authenticate({
        token: payload.token,
        imgUrl: payload.imgUrl,
        userName: payload.username,
      })
    );
    localStorage.setItem("token", token);
    const remainingTime = payload.exp * 1000 - new Date().getTime();
    setTimeout( () => {
        localStorage.removeItem('token');
        dispatch(authActions.logout())
      }, remainingTime)
    history.push("/welcome");
  } else {
    const error = params.get("error");
    dispatch(authActions.errorAdquired(error));
  }

  return <Fragment />;
};

export default Callback;
